const path = require('path');

module.exports = {
  entry: './src/main.ts',
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    port: 9000
  },
  module: {
    rules: [
      {
        test: /\.(png|jpg)$/,
        exclude: /node_modules/,
        use: 'file-loader'
      },
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [
          'ts-loader',
          'tslint-loader'
        ]
      }
    ]
  },
  resolve: {
    extensions: [ '.ts', '.js' ]
  }
};
