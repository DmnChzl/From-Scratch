import thumb from './thumb.png';

const { ...data } = ['Hello', 'World', '!']; // ES2018

const concat = (...values: string[]) => values.reduce((result: string, value: string) => result += ` ${value}`, '');

const createTitle = () => `
  <span>${concat(data[0], data[1], data[2])}</span>
  <img style="height: 1.5em;" src="${thumb}">
`;

export default createTitle();
